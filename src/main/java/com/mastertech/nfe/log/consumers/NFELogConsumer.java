package com.mastertech.nfe.log.consumers;

import com.mastertech.nfe.cadastro.models.dtos.NFELogDTO;
import com.mastertech.nfe.log.services.NFELogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class NFELogConsumer {

    @Autowired
    private NFELogService service;

    @KafkaListener(topics = "spec2-william-pimentel-2", groupId = "nfe-log")
    public void receber(@Payload NFELogDTO nfeLogDTO) throws IOException {
        System.out.println("Inicio consumer log");
        service.log(nfeLogDTO);
        System.out.println("Fim consumer log");
    }
}
