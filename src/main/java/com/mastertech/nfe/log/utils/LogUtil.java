package com.mastertech.nfe.log.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LogUtil {

    public static void gravaNoArquivo(String data) throws IOException {
        String fileName = "/home/a2/Documentos/logs/nfe.log";
        File file = new File(fileName);

        if (!file.exists()) {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(data);
            writer.newLine();
            writer.close();
        } else {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.append(data);
            writer.newLine();
            writer.close();
        }
    }
}
