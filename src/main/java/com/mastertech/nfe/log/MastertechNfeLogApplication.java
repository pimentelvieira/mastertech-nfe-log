package com.mastertech.nfe.log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MastertechNfeLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastertechNfeLogApplication.class, args);
	}

}
