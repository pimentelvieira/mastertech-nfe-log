package com.mastertech.nfe.log.services;

import com.mastertech.nfe.cadastro.models.dtos.NFELogDTO;
import com.mastertech.nfe.log.utils.LogUtil;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

@Service
public class NFELogService {

    public void log(NFELogDTO nfeLogDTO) throws IOException {
        String msg;
        if(nfeLogDTO.getLogType().equals("Emissão")) {
            msg = "[" + new Date() + "]" + "[Emissão]: " + nfeLogDTO.getIdentidade() + " acaba de pedir a emissão de uma NF no valor de R$ " + nfeLogDTO.getValor();
        } else {
            msg = "[" + new Date() + "]" + "[Consulta]: " + nfeLogDTO.getIdentidade() + " acaba de pedir os dados das suas notas fiscais";
        }

        LogUtil.gravaNoArquivo(msg);
    }
}
